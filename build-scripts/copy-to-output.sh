#!/bin/bash

## This script copies the build output to the output dir
## so it can be used by hekate

cd ${BUILDBASE}

# set gapps default to pico
GAPPS="pico"

ZIP_FILE=$(ls ./android/lineage/out/target/product/$ROM_NAME/lineage-17.1-*-UNOFFICIAL-$ROM_NAME.zip | tail -1)

## Copy to output
echo "Creating switchroot install dir..."
mkdir -p ./android/output/switchroot/install
echo "Creating switchroot android dir..."
mkdir -p ./android/output/switchroot/android
echo "Downloading hekate..."
LATEST_HEKATE=$(curl -sL https://github.com/CTCaer/hekate/releases/latest | grep -o '/CTCaer/hekate/releases/download/.*/hekate_ctcaer.*zip')
curl -L -o ./hekate.zip https://github.com/$LATEST_HEKATE
unzip -u ./hekate.zip -d ./android/output/
echo "Copying build zip to SD Card..."
cp $ZIP_FILE ./android/output/
echo "Copying build combined kernel and ramdisk..."
cp ./android/lineage/out/target/product/$ROM_NAME/boot.img ./android/output/switchroot/install/
echo "Copying build dtb..."
cp ./android/lineage/out/target/product/$ROM_NAME/obj/KERNEL_OBJ/arch/arm64/boot/dts/tegra210-icosa.dtb ./android/output/switchroot/install/
echo "Downloading twrp..."
curl -L -o ./android/output/switchroot/install/twrp.img https://github.com/PabloZaiden/switchroot-android-build/raw/master/external/twrp.img

# swap gapps type to tvmini for atv
if [[ $ROM_NAME = "foster" ]]; then
	GAPPS="tvmini"
fi

echo "Downloading $GAPPS Open GApps..."

# get base URL for gapps
BASE_GAPPS_URL=$(curl -L https://sourceforge.net/projects/opengapps/rss?path=/arm64 \
	| grep -Po "https:\/\/.*10\.0-$GAPPS.*zip\/download" \
	| head -n 1 \
	| sed "s/\/download//" \
	| sed "s/files\///" \
	| sed "s/projects/project/" \
	| sed "s/sourceforge/downloads\.sourceforge/")

TIMESTAMP=$(echo $(( $(date '+%s%N') / 1000000000)))
FULL_GAPPS_URL=$(echo $BASE_GAPPS_URL"?use_mirror=autoselect&ts="$TIMESTAMP)
curl -L -o ./android/output/opengapps_$GAPPS.zip $FULL_GAPPS_URL
