#!/bin/bash

JOBS=$(($(nproc) - 1)) # Google guidelines for `repo`

cd ${BUILDBASE}/android/lineage
if [[ $ROM_BUILD == "maru" ]]; then
	repo init -u https://gitlab.com/switchroot-maru-os/manifest.git -b maru-0.8  --no-clone-bundle --depth=1
	repo sync --force-sync -j${JOBS}
elif [[ $ROM_BUILD == "lineage" ]]; then
	repo init -u https://github.com/LineageOS/android.git -b lineage-17.1
	repo sync --force-sync -j${JOBS}

	cd ${BUILDBASE}/android/lineage/.repo
	git clone https://gitlab.com/switchroot/android/manifest.git -b lineage-17.1 local_manifests

	repo sync --force-sync -j${JOBS}
else
	echo "Couldn't recognize build type. Choices: maru | lineage"
fi
